﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nsConsoleApp03_MathUr_Manager
{
    class HandlerClass
    {
        public Calculator calculator;
        public HandlerClass()
        {
            calculator = new Calculator();
        }

        public void setDefinition(int definition)
        {
            calculator.Definition = definition;
        }

        public int getType()
        {
            return calculator.getTypeUr();
        }
        public bool isAnswer()
        {
            return calculator.getDecision();
        }
        public void getAnswer(out double x1,out double x2)
        {
            int type; // type - тип уравнения. Доступные значения:
                      // 0 - пустое уравнение (a,b,c=0)
                      // 1 - полный квадратный трехчлен
                      // 2 - линейное уравнение (a = 0)
                      // 3 - квадратное уравнения (b = 0)
                      // 4 - уравнение, не имеющее решений (a,b=0, c!=0)
            x1 = Double.NaN;
            x2 = Double.NaN;
            
            type = calculator.getTypeUr();
            switch (type)
            {
                case 0:
                    break;
                case 1:
                    calculator.calPolKvUr();
                    x1 = calculator.X1;
                    x2 = calculator.X2;
                    break;
                case 2:
                    calculator.calLinUr();
                    x1 = calculator.X1;
                    break;
                case 3:
                    calculator.calKvUr();
                    x1 = calculator.X1;
                    break;
                case 4:
                    break;
            }
        }

        // Проверка задания коэффициентов
        // Возвращает массив bool (true - значение уже задано)
        public bool[] IsEntered()
        {
            return new bool[]
            {
                !Double.IsNaN(calculator.A),
                !Double.IsNaN(calculator.B),
                !Double.IsNaN(calculator.C)
            };
        }

        public bool IsEmpty()
        {
            return (Double.IsNaN(calculator.A) &&
                Double.IsNaN(calculator.B) &&
                Double.IsNaN(calculator.C));
        }
        public bool IsFull()
        {
            return !(Double.IsNaN(calculator.A) ||
                Double.IsNaN(calculator.B) ||
                Double.IsNaN(calculator.C));
        }

        public void EnterAbc(int number_k, double k)
        {
            switch(number_k){
                case 1:
                    calculator.A = k;
                    break;
                case 2:
                    calculator.B = k;
                    break;
                case 3:
                    calculator.C = k;
                    break;
            }
        }

        public double getA()
        {
            return calculator.A;
        }

        public double getB()
        {
            return calculator.B;
        }

        public double getC()
        {
            return calculator.C;
        }
    }
}
