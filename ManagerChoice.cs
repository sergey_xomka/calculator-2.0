﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nsConsoleApp03_MathUr_Manager
{
    class ManagerChoice
    {
        List<String> current_list;
        Dictionary<String, String> choice_collection = new Dictionary<String, string>()
            {
                { "enter_abc" , "Я хочу ввести один из коэффициентов a, b, c" },
                { "enter_a" , "Я хочу ввести коэффициент a" },
                { "enter_b" , "Я хочу ввести коэффициент b" },
                { "enter_c" , "Я хочу ввести коэффициент c" },
                { "edit_a" , "Я хочу заменить коэффициент a" },
                { "edit_b" , "Я хочу заменить коэффициент b" },
                { "edit_c" , "Я хочу заменить коэффициент c" },
                { "back" , "Я хочу сделать что-то другое" }, 
                { "see_abc", "Хочу посмотреть текущие коэффициенты" }, 
                { "see_type", "Какой тип получившегося уравнения?" },
                { "equation" , "Что за уравнение получилось?"},
                { "solution" , "Покажите мне решение введенного уравнения"},
                { "definition" , "Хочу указать точность вычислений"},
                { "can_be" , "Что можно сделать?"},
                { "wait", "Я пока подумаю, спасибо." },
                { "exit", "Ни один из вариантов меня не устраивает, я ухожу." }
            };

        
        public List<String> Current_list
        {
            get
            {
                return current_list;
            }
            set
            {
                current_list = value;
            }
        }

        // Стандартное задание доступных вариантов ответа
        public ManagerChoice()
        {
            current_list = new List<string>();
            current_list.Add("can_be");
            current_list.Add("wait");
            current_list.Add("exit");
        }

        // Вывод вариантов ответа
        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("Доступные варианты ответа:");
            for (int i = 0, k = 1; i < current_list.Count; i++, k++)
            {
                stringBuilder.Append($"\n {k}) \"{choice_collection[current_list[i]]}\"");
            }
            return stringBuilder.ToString();
        }

        // Возврат string-ключа выбранного варианта
        public string getChoiceInList(int i)
        {
            return current_list[i];
        }

        // Возврат string-значения выбранного варианта
        public string getValueChoice(string key)
        {
            return choice_collection.ContainsKey(key) ?  choice_collection[key] : string.Empty;
        }
    }
}
