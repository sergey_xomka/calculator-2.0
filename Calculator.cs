﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nsConsoleApp03_MathUr_Manager
{
    class Calculator
    {
        double a,b,c; // Коэффициенты уравнения
        double x1, x2; // Корни уравнения
        Boolean decision = false; // Отсутствие корней в поле действительных чисел
        int definition; // Степень точности
                       
        
        public double A
        {
            get
            {
                return a;
            }
            set
            {
                a = value;
            }
        }

        public double B
        {
            get
            {
                return b;
            }
            set
            {
                b = value;
            }
        }
        public double C
        {
            get
            {
                return c;
            }
            set
            {
                c = value;
            }
        }
        
        public double X1
        {
            get
            {
                return Math.Round(x1, definition);
            }
            set
            {
                x1 = value;
            }
        }

        public double X2
        {
            get
            {
                return Math.Round(x2, definition);
            }
            set
            {
                x2 = value;
            }
        }

        public Calculator()
        {
            this.a = Double.NaN;
            this.b = Double.NaN;
            this.c = Double.NaN;
        }

        // definition - числовое поле, указывающее точность вычислений -
        // количество знаков после запятой
        public int Definition
        {
            get
            {
                return definition;
            }
            set
            {
                definition = value;
            }
        }


        public int getTypeUr()
        {

            if ((a + b + c) == 0)
            {
                return 0; // 0 - пустое уравнение (a,b,c=0)
            } else if ((a + b) == 0 && c != 0)
            {
                return 4; // 4 - уравнение, не имеющее решений (a,b=0, c!=0)
            } else if (a == 0)
            {
                return 2; // 2 - линейное уравнение (a = 0)
            } else if (b == 0)
            {
                return 3; // 3 - квадратное уравнения (b = 0)
            } else
            {
                return 1; // 1 - полный квадратный трехчлен
            }
        }
        
        // Подсчёт корней полного квадратного уравнения 
        public void calPolKvUr()
        {
            decision = true;
            double d = b*b-4*a*c;
            if (d > 0)
            {
                x1 = ((-b) - Math.Sqrt(d)) / (2 * a);
                x2 = ((-b) + Math.Sqrt(d)) / (2 * a);
            } else if (d == 0)
            {
                x1=x2=( -b )/(2 * a);
            } else if (d < 0)
            {
                decision = false;
            }
        }

        // Подсчёт корней квадратного уравнения x^2 = -c / a
        // return boolean : false корней нет; true - корни есть;
        public void calKvUr()
        {
            if (((-c / a) < 0))
            {
                x1 = Double.NaN;
                decision = false;
            }
            else
            {
                x1 = Math.Sqrt(-c / a);
                decision = true;
            }
            
        }

        // Подсчёт корней линейного уравнения bx + c = 0
        public void calLinUr()
        {
            decision = true;
            x1 = -c / b;
        }

        public bool getDecision()
        {
            return decision;
        }

    }
}
