﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
namespace nsConsoleApp03_MathUr_Manager
{
    class Manager
    {
        Timer timer;
        ManagerChoice mChoice;
        string NameManager; // Имя менеджера
        Boolean defaultCanBe = false;
        HandlerClass hClass;

        public Manager()
        {
            this.NameManager = "[Менеджер Лика]: ";
            this.timer = new Timer(10000);
            this.timer.Elapsed += OnTimerEvent;
            this.hClass = new HandlerClass();
        }
        // Конструктор с параметрами
        // NameManager - имя менеджера
        // timer - время (в мс), через которое менеджер снова спрашивает
        public Manager(String NameManager, int timer)
        {
            this.NameManager = $"[Менеджер {NameManager}]: ";
            this.timer = new Timer(timer);
            this.timer.Elapsed += OnTimerEvent;
        }

        public void StartManager()
        {
            // Приветствуем пользователя
            Greeting(true); 
            // Создание экземпляра класса, 
            // обрататывающего варианты выбора
            mChoice = new ManagerChoice();
            Console.WriteLine(mChoice);
            string c;
            while (!(c =WhiteEnter()).Equals("exit"))
            {
                KeyHandler(c);
            }
        }


        //  Случайное приветствие, вопрос о желании сделать что-то
        public void Greeting(Boolean first)
        {
            String[] hello_one = new string[] {
                "Добро пожаловать!",
                "Здравствуйте.",
                "Приветствую вас в калькуляторе, призванном помочь в подсчёте корней квадратных уравнений!",
                "Добро пожаловать в счётную систему Calculator.",
                "Добрый день!"};
            String[] hello_two = new string[]
            {
                "Один из первых известных выводов формулы корней принадлежит индийскому учёному Брахмагупте: он изложил универсальное правило решения квадратного уравнения, приведённого к каноническому виду.",
                "Уже во втором тысячелетии до нашей эры вавилоняне знали, как решать квадратные уравнения.",
                "Мало кто помнит теорему Виета после окончания школы, еще меньше людей знают о том, что она касается решения не только квадратных уравнений. Даже данный калькулятор использует классическую схему решения с помощью дискриминанта.",
                "Французский математик Франсуа Виет во время войны между Францией и Испанией смог склонить чашу весов в сторону Франции, расшифровав код из более чем 600 символов, с помощью которого испанцы вели переписку с союзниками. За это церковь обвинила Виета в связях с нечистой силой, и он чудом избежал сожжения на костре."
            };
            String[] hello = new String[] {"Что желаете сделать?",
                "Вам чем-нибудь помочь?",
                "Что вы хотите сделать?",
                "Я могу быть чем-то полезен?",
                "Я менеджер данного калькулятора, чем я могу вам помочь?"};
            int hello_number = (new Random()).Next(hello.Length);
            if (first)
            {
                Console.WriteLine($"{NameManager}{hello_one[(new Random()).Next(hello_one.Length)]} {hello[hello_number]}");
            }
            else
            {
                Console.WriteLine($"{NameManager}Интересный факт! \n {hello_two[(new Random()).Next(hello_two.Length)]} \n {hello[hello_number]}");
            }
        }

        public void Incomprehension()
        {
            String[] inc = new String[] {"Я не понимаю, что вы хотели этим сказать?",
                "Что? Повторите, пожалуйста.",
                "Что это значит?",
                "Извините, я не понимаю :( Повторите снова. " };
            int inc_number = (new Random()).Next(inc.Length);
            Console.WriteLine($"{NameManager}{inc[inc_number]}");
        }

        private String WhiteEnter()
        {
            while (true)
            {
                String enterChoise = Console.ReadLine();
                if (!timer.Enabled)
                {
                    if ((int.TryParse(enterChoise, out int c))
                        && !(mChoice.Current_list.Count < c))
                    {
                        return (mChoice.getChoiceInList(c - 1));
                    }
                    else
                    {
                        Incomprehension();
                        Console.WriteLine(mChoice);
                    }
                }
                else
                {
                    timer.Enabled = false;
                    Incomprehension();
                    Console.WriteLine(mChoice);
                }
            }
        }


        private void KeyHandler(string key)
        {
            switch (key)
            {
                case "exit":
                    break;
                case "wait":
                    timer.Enabled = true;
                    break;
                case "back":
                    UpdateChoice();
                    Console.WriteLine($"{NameManager}{mChoice}");
                    break;
                case "can_be":
                    UpdateChoice();
                    Console.WriteLine($"{NameManager}{mChoice}");
                    break;
                case "enter_abc":
                    EnterAbc(0);
                    break;
                case "enter_a":
                    EnterAbc(1);
                    break;
                case "edit_a":
                    EnterAbc(1);
                    break;
                case "enter_b":
                    EnterAbc(2);
                    break;
                case "edit_b":
                    EnterAbc(2);
                    break;
                case "edit_c":
                    EnterAbc(3);
                    break;
                case "enter_c":
                    EnterAbc(3);
                    break;
                case "see_abc":
                    See_abc();
                    break;
                case "see_type":
                    SeeType();
                    break;
                case "equation":
                    Equation();
                    break;
                case "solution":
                    Solution();
                    break;
                case "definition":
                    Definition();
                    break;
            }
        }

        private void OnTimerEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            timer.Enabled = false;
            Greeting(false);
            Console.WriteLine(mChoice);
        }

        // Получить список доступных действий
        private void UpdateChoice()
        {

            // При первом вызове показываем стандартный список
            if (defaultCanBe) 
            {
                mChoice.Current_list = new List<string> {
                    "enter_abc",
                    "can_be",
                    "wait",
                    "exit"
                };
                Console.WriteLine($"{NameManager}{mChoice}");
                defaultCanBe = false;
                return;
            }
            List<string> listChoice = new List<string>();

            if (hClass.IsEmpty())
            {
                listChoice.Add("enter_abc");
            }
            else
            {
                bool[] isEntered = hClass.IsEntered();
                String abc = "abc";
                for (int i = 0; i < 3; i++)
                {
                    listChoice.Add(isEntered[i] ? $"edit_{abc[i]}" : $"enter_{abc[i]}");
                }
                if (isEntered.Contains(true)) listChoice.Add("see_abc");
                if (hClass.IsFull())
                {
                    listChoice.Add("see_type");
                    listChoice.Add("equation");
                    listChoice.Add("solution");
                }
            }
            listChoice.Add("definition");
            listChoice.Add("wait");
            listChoice.Add("exit");
            mChoice.Current_list = listChoice;
        }

        // Выбор коэффициента для ввода и
        // ввод коэффициентов a,b,c
        // number_k:
        // 1 - ввод a, 2 - ввод b, 3 - ввод c
        // любое другое - выбор коэффициента
        private void EnterAbc(int number_k)
        {
            // Количество повторных попыток распознать коэффициент
            const int k = 3;
            switch (number_k) {
                default:
                    Console.WriteLine($"{NameManager}Какой коэффициент вы хотите ввести?");
                    mChoice.Current_list = new List<string>
                    {
                        "enter_a",
                        "enter_b",
                        "enter_c",
                        "back",
                        "wait",
                        "exit"
                    };
                    Console.WriteLine(mChoice);
                    break;
                case 1:
                    Console.WriteLine($"{NameManager}Введите коэффициент a");
                    double ka;
                    for (int i = 0; i < k; i++)
                    {
                        if (i > 0) Console.WriteLine("Необходимо ввести коэффициент a");
                        if (double.TryParse(Console.ReadLine(), out ka))
                        {
                            hClass.EnterAbc(number_k, ka);
                            UpdateChoice();
                            Console.WriteLine($"{NameManager} Коэффициент а={ka} успешно введён. Что-то еще?\n{mChoice}");
                            return;
                        }
                        Incomprehension();
                    }
                    Console.WriteLine(mChoice);
                    break;
                case 2:
                    Console.WriteLine($"{NameManager}Введите коэффициент b");
                    double kb;
                    for (int i = 0; i < k; i++)
                    {
                        if (i > 0) Console.WriteLine("Необходимо ввести коэффициент b");
                        if (double.TryParse(Console.ReadLine(), out kb))
                        {
                            hClass.EnterAbc(number_k, kb);
                            UpdateChoice();
                            Console.WriteLine($"{NameManager} Коэффициент b={kb} успешно введён. Что-то еще?\n{mChoice}");
                            return;
                        }
                        Incomprehension();
                    }
                    Incomprehension();
                    break;
                case 3:
                    Console.WriteLine($"{NameManager}Введите коэффициент c");
                    double kc;
                    for (int i = 0; i < k; i++)
                    {
                        if (i > 0) Console.WriteLine("Необходимо ввести коэффициент c");
                        if (double.TryParse(Console.ReadLine(), out kc))
                        {
                            hClass.EnterAbc(number_k, kc);
                            UpdateChoice();
                            Console.WriteLine($"{NameManager} Коэффициент c={kc} успешно введён. Что-то еще?\n{mChoice}");
                            return;
                        }
                        Incomprehension();
                    }
                    Incomprehension();
                    break;
            }
        }
        private void See_abc()
        {
            Boolean[] isEntered = hClass.IsEntered();
            StringBuilder sB1 = new StringBuilder();
            StringBuilder sB2 = new StringBuilder();
            sB1.Append("Введены следующие коэффициенты:");
            if(isEntered[0])
            {
                sB1.Append($"\n{String.Empty,5}a={ hClass.getA()}");
            }
            else
            {
                sB2.Append($"\n{String.Empty,5}a не введён");
            }
            if (isEntered[1])
            {
                sB1.Append($"\n{String.Empty,5}b={ hClass.getB()}");
            }
            else
            {
                sB2.Append($"\n{String.Empty,5}b не введён");
            }
            if (isEntered[2])
            {
                sB1.Append($"\n{String.Empty,5}c={ hClass.getC()}");
            }
            else
            {
                sB2.Append($"\n{String.Empty,5}c не введён");
            }
            sB1.Append(sB2);
            Console.WriteLine(sB1);
            Console.WriteLine(mChoice);
        }
        private void SeeType() {
            // 1 - полный квадратный трехчлен
            // 2 - линейное уравнение (a = 0)
            // 3 - квадратное уравнения (b = 0)
            // 4 - уравнение, не имеющее решений (a,b=0, c!=0)

            switch (hClass.getType())
            {
                case 0:
                    Console.WriteLine($"{NameManager}Все коэффициенты равны нулю.");
                    break;
                case 1:
                    Console.WriteLine($"{NameManager}Задано полное квадратное уравнение.");
                    break;
                case 2:
                    Console.WriteLine($"{NameManager}Коэффициент a=0. Задано линейное квадратное уравнение.");
                    break;
                case 3:
                    Console.WriteLine($"{NameManager}Коэффициент b=0. Задано неполное квадратное уравнение.");
                    break;
                case 4:
                    Console.WriteLine($"{NameManager}Коэффициенты a,b=0, c!=0. Уравнение не имеет решений.");
                    break;
            }
            Console.WriteLine(mChoice);
        }

        // Узнать заданное уравнение
        private void Equation()
        {
            String sB = (Math.Sign(hClass.getB()) == 1) ? "+" : "";
            String sC = (Math.Sign(hClass.getC()) == 1) ? "+" : "";
            Console.WriteLine($"{NameManager}Задано уравнение {hClass.getA()}x^2{sB}{hClass.getB()}x{sC}{hClass.getC()}=0");
            Console.WriteLine(mChoice);
        }
        

        // Получение решения уравнения
        private void Solution()
        {
            double x1;
            double x2;
            String[] type = new String[] {
                "пустое уравнение",
                "полный квадратный трёхчлен",
                "линейное уравнение",
                "квадратное уравнение",
                "уравнение без решений"
            };
            

            hClass.getAnswer(out x1, out x2);

            StringBuilder sb = new StringBuilder();
            String sB = (Math.Sign(hClass.getB()) == 1|| Math.Sign(hClass.getB()) == 0)? "+" : "";
            String sC = (Math.Sign(hClass.getC()) == 1|| Math.Sign(hClass.getB()) == 0)? "+" : "";
            sb.Append($"{NameManager}Задано уравнение {hClass.getA()}(x^2){sB}{hClass.getB()}x{sC}{hClass.getC()}=0");
            sb.Append($"\nТип: "+type[hClass.getType()]);

            switch (hClass.getType())
            {
                case 1:
                    if (hClass.isAnswer())
                    {
                        if (Double.IsNaN(x2))
                        {
                            sb.Append($"\nДискриминант равен нулю. Корень один:");
                            sb.Append($"\nx1= " + x1);
                            sb.Append($"\nТочность вычисления: {hClass.calculator.Definition} знаков после запятой");
                        }
                        else
                        {
                            sb.Append($"\nДискриминант положительный. Корня два:");
                            sb.Append($"\nx1= " + x1);
                            sb.Append($"\nx2= " + x2);
                            sb.Append($"\nТочность вычисления: {hClass.calculator.Definition} знаков после запятой");
                        }
                    }
                    else
                    {
                        sb.Append($"\nДискриминант отрицательный. Корней нет.");
                    }
                    break;
                case 2:
                    sb.Append($"\nКорень уравнения:");
                    sb.Append($"\nx1= " + x1);
                    sb.Append($"\nТочность вычисления: {hClass.calculator.Definition} знаков после запятой");
                    break;
                case 3:
                    if (hClass.isAnswer())
                    { 
                        sb.Append($"\nКорень уравнения:");
                        sb.Append($"\nx1=+" + x1);
                        sb.Append($"\nx2=-" + x1);
                        sb.Append($"\nТочность вычисления: {hClass.calculator.Definition} знаков после запятой");
                    }
                    else
                    {
                        sb.Append($"\nПодкоренное значение отрицательное. Корней нет.");
                    }
                    break;
                case 4:
                        sb.Append($"\nКорней нет.");
                    break;
            }
            Console.WriteLine(sb);
            Console.WriteLine(mChoice);
        }

        // Задание точности подсчёта корней 
        public void Definition()
        {
            // Количество повторных попыток распознать число
            const int k = 3;
            Console.WriteLine($"{NameManager}Введите количество знаков после запятой (от 1 до 10)");
            int d;
            for (int i = 0; i < k; i++)
            {
                if (i > 0) Console.WriteLine("Необходимо количество знаков после запятой (от 1 до 10)");
                if (int.TryParse(Console.ReadLine(), out d))
                {
                    if (d < 11 && d > 0)
                    {
                        hClass.setDefinition(d);
                        Console.WriteLine($"{NameManager} Точность в {d} знаков после запятой установлена \n{mChoice}");
                        return;
                    }
                }
                Incomprehension();
            }
            Incomprehension();
        }
    }
    
}
